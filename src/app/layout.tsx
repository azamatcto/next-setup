import '@/styles/index.scss'
import { ReactNode } from 'react'
import type { Metadata } from 'next'
import { Inter as FontSans } from 'next/font/google'
import { cn } from '@/shared/ui/utils'

const fontSans = FontSans({
  subsets: ['latin'],
  variable: '--font-sans'
})

type RootLayoutProps = {
  children: ReactNode
}

export const metadata: Metadata = {
  title: '',
  description: ''
}

function RootLayout({ children }: RootLayoutProps) {
  return (
    <html lang="en" suppressHydrationWarning>
      <body className={cn('font-sans', fontSans.variable)}>{children}</body>
    </html>
  )
}

export default RootLayout
